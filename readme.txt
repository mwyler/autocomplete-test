== Installation ==
1. Upload the folder "autocomplete-test" to "/wp-content/plugins/"
2. Activate the plugin through the "Plugins" menu in WordPress
3. use shortcode to call search from in page or post:
[autocomplete-test]
In PHP template use call shortcode using below function
<?php do_shortcode('[autocomplete-test]'); ?>
