<?php
/*
Plugin Name: Autocomplete Test
Description: Create a wordpress plugin for all of the above in which it looks like it's a part of the current template but is accessible through /location-search/ URL.
Author: Mauricio Wyler
Version: 1.0.0
*/

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

define('WP_DEBUG', true);

define('AUTOCOMPLETE_TEST_DIR', plugin_dir_path(__FILE__));
define('AUTOCOMPLETE_TEST_URL', plugin_dir_url(__FILE__));
define('AUTOCOMPLETE_TEST_INCLUDE_DIR', plugin_dir_path(__FILE__) . 'pages/');
define('AUTOCOMPLETE_TEST_INCLUDE_URL', plugin_dir_url(__FILE__) . 'pages/');
define('AUTOCOMPLETE_TEST_SQL_DIR', plugin_dir_path(__FILE__) . 'sql/');

if (!defined('WPCMN_VER'))
    define('WPCMN_VER', '1.0.0');


// Start up the engine
class WP_Autocomplete_Test
{
    private $tableName = "autocomplete_test";

    /**
     * Static property to hold our singleton instance
     *
     */
    static $instance = false;

    /**
     * This is our constructor
     *
     * @return void
     */
    private function __construct()
    {

        register_activation_hook(__FILE__, array($this, 'createTable'));
        register_activation_hook(__FILE__, array($this, 'populate'));

        // back end
        add_action('plugins_loaded', array($this, 'textdomain'));

        // front end
        add_action('wp_enqueue_scripts', array($this, 'front_scripts'), 10);
        add_action('wp_ajax_autocomplete', array($this, 'autocomplete_callback'));
        add_action('wp_ajax_nopriv_autocomplete', array($this, 'autocomplete_callback'));

        add_shortcode('autocomplete-test', array($this, 'autocomplete_test_shortcode'));
    }

    /**
     * If an instance exists, this returns it.  If not, it creates one and
     * retuns it.
     *
     * @return WP_Autocomplete_Test
     */

    public static function getInstance()
    {
        if (!self::$instance)
            self::$instance = new self;
        return self::$instance;
    }

    /**
     * load textdomain
     *
     * @return void
     */

    public function textdomain()
    {

        load_plugin_textdomain('actest', false, dirname(plugin_basename(__FILE__)) . '/languages/');
    }

    public function createTable()
    {

        global $wpdb;

        $table_name = $wpdb->prefix . $this->tableName;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `location` varchar(150) NOT NULL,
              `slug` varchar(150) NOT NULL,
              `population` int(10) unsigned NOT NULL,
              PRIMARY KEY (`id`),
              KEY `location` (`location`)
            ) $charset_collate;";

        dbDelta($sql);
    }

    public function populate()
    {

        global $wpdb;

        $tableName = $wpdb->prefix . $this->tableName;
        $file = AUTOCOMPLETE_TEST_SQL_DIR . 'data.csv';

        if (($handle = fopen($file, "r")) !== FALSE) {
            while (($row = fgetcsv($handle, null, "\t")) !== FALSE) {
                $wpdb->insert(
                    $tableName,
                    array(
                        'location' => $row[1],
                        'slug' => $row[2],
                        'population' => $row[3],
                    )
                );
            }
            fclose($handle);
        }
    }

    public function front_scripts()
    {
        wp_enqueue_style('typeahead_css', plugins_url('includes/typeahead.css', __FILE__));
        wp_enqueue_script('typeahead_js', plugins_url('includes/typeahead.bundle.min.js', __FILE__), array('jquery'), '', false);

        wp_enqueue_style('autocomplete_test_css', plugins_url('includes/autocomplete.css', __FILE__));

        $wp_typeahead_vars = array('ajaxurl' => admin_url('admin-ajax.php'));
        wp_localize_script('typeahead_js', 'autocompleteTest', $wp_typeahead_vars);

    }

    public function autocomplete_test_shortcode()
    {
        $html = '<div id="remote">
                  <input class="typeahead" type="text" placeholder="Find it here...">
                  <img src="' . plugins_url('images/spinner.gif', __FILE__) . '" class="autocomplete-spinner">
                </div>';
        $script = " <script>
                    function setLoading(value) {
                        var spinner = jQuery('.autocomplete-spinner', '#remote');

                        if(value) {
                            spinner.show();
                        } else {
                            spinner.hide();
                        }
                    }

                    function navigate(slug) {
                        window.location.href = '/' + slug;
                    }

                    var locations = new Bloodhound({
                      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                      queryTokenizer: Bloodhound.tokenizers.whitespace,
                      remote: {
                        url: autocompleteTest.ajaxurl + '?action=autocomplete&terms=%QUERY',
                        wildcard: '%QUERY'
                      }
                    });

                    jQuery('#remote .typeahead').typeahead({
                        minLength: 3
                    }, {
                      name: 'autocomplete-test',
                      display: 'value',
                      source: locations
                    }).bind('typeahead:asyncrequest', function() {
                        setLoading(true);
                    }).bind('typeahead:asynccancel', function() {
                        setLoading(false);
                    }).bind('typeahead:asyncreceive', function() {
                        setLoading(false);
                    }).bind('typeahead:autocomplete', function(e, suggestion) {
                        navigate(suggestion.slug);
                    }).bind('typeahead:select', function(e, suggestion) {
                        navigate(suggestion.slug);
                    });
                    </script>";

        return $html . $script;
    }

    public function autocomplete_callback()
    {
        global $wpdb;

        if (!empty($_REQUEST['terms'])) {

            $searchString = sanitize_text_field($_REQUEST['terms']);

            $tableName = $wpdb->prefix . $this->tableName;
            $locations = $wpdb->get_results(
                "   SELECT *
                    FROM {$tableName}
                    WHERE (location LIKE '%{$searchString}' OR location LIKE '%{$searchString}%')"
            );

            $result = array();
            if ($locations) {
                foreach ($locations as $location) {
                    $result[] = array('value' => $location->location, 'slug' => $location->slug);
                }
            }

            echo json_encode($result);
        }
        die;
    }
}


// Instantiate our class
$WP_Autocomplete_Test = WP_Autocomplete_Test::getInstance();